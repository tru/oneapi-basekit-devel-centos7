FROM registry-gitlab.pasteur.fr/tru/os-tools-centos7:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

# adpated from https://github.com/intel/oneapi-containers/blob/master/images/docker/basekit-devel-centos8/Dockerfile
# https://software.intel.com/content/www/us/en/develop/documentation/installation-guide-for-intel-oneapi-toolkits-linux/top/installation/install-using-package-managers/yum-dnf-zypper.html
RUN yum -y install \
intel-basekit

# intel-basekit-getting-started \
# intel-oneapi-advisor \
# intel-oneapi-ccl-devel \
# intel-oneapi-common-licensing \
# intel-oneapi-common-vars \
# intel-oneapi-compiler-dpcpp-cpp \
# intel-oneapi-dal-devel \
# intel-oneapi-dev-utilities \
# intel-oneapi-dnnl-devel \
# intel-oneapi-dpcpp-debugger \
# intel-oneapi-ipp-devel \
# intel-oneapi-ippcp-devel \
# intel-oneapi-libdpstd-devel \
# intel-oneapi-mkl-devel \
# intel-oneapi-onevpl-devel \
# intel-oneapi-python \
# intel-oneapi-tbb-devel \
# intel-oneapi-vtune \
# --
